/**
 *  @author   Fernando Salazar <fernando@blufish.com>
 *  @author   James Gov <james@blufish.com>
 *  @since    Tuesday, September 09, 2016
 */

"use strict";

var through     = require('through2'),
	concat      = require('concat-stream'),
	gUtil       = require('gulp-util'),
	fs          = require('fs'),
	extend      = require('extend'),
	map         = require('map-stream'),
	vfs         = require('vinyl-fs'),
	inject      = require('gulp-inject'),
	gulp        = require('gulp'),
	streamifier = require('streamifier'),
	btv         = require('buffer-to-vinyl'),
	path        = require('path'),
	include     = require('gulp-file-include');

module.exports = function(html) {
	var data,
		self;

	/**
	 *  @param    object file, string encoding, function cb
	 *  @return   void
	 */
	function fresh(file, encoding, cb) {
		self = this;

		switch(true) {

			/**
			 *  Pass along null file.
			 */
			case file.isNull():
				cb(null, file);
				break;

			/**
			 *  @todo   support stream
			 */
			case file.isStream():
				file.contents.pipe(concat(function(data) {
					try {
						file.contents = new Buffer(_parse(String(file.contents)));

						cb(null, file);
					} catch(e) {
						cb(new gUtil.PluginError('gulp-fresh', e.message));
					}
				}));
				break;

			/**
			 *  Process file
			 */
			case file.isBuffer():
				try {
					data = JSON.parse(String(file.contents));
					html.pipe(map(_each));

					// cb(null, file);
				} catch (e) {
					cb(new gUtil.PluginError('gulp-fresh', e.message));
				}

				break;
		}

	}

	function _each(file, cb) {
		var isWin = process.platform === "win32";
		var directorySeparator = "/";
		if (isWin)
		{
			directorySeparator = "\\";
		}
		var split    = file.path.split(directorySeparator),
			que      = null,
			param    = null,
			js       = [],
			insertJs = [],
			i, stream, streamCopy;

		split.forEach(function(index){
			if(que !== null) {
				param = index;
				que   = null;
			}

			if(index == 'dist')
				que = 'dist';
		});

		if(param == 'index.html')
		{
			param = 'index';
		}

		if (param)
		{
			data[param].scripts.forEach(function(index){
				js.push(index['src']);
			});
	
			for(i=0; i<js.length; i++){
				var jsPath = js[i].substr(js[i].indexOf("scripts"));
	
				insertJs.push('dist/'+jsPath);
			}
	
			streamCopy = gulp.src(js, {base: 'app'})
						.pipe(gulp.dest('dist/'))
						.on('end', function() {
							stream = inject(gulp.src(insertJs, {read: false}));
								stream.once('data', function(newFile) {
									file.contents = newFile.contents;
									self.push(file);
								});
							stream.write(file);
						});
		}
	}

	return through.obj(fresh);
}
